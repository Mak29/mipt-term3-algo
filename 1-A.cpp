#include <functional>
#include <iostream>
#include <string>
#include <vector>

template <typename Output>
class SubstringsFinder {
 public:
  SubstringsFinder(const std::string& pattern, const Output& out)
      : out_(out),
        pattern_(pattern),
        buffer_(pattern_.size() + 1, 0),
        z_(pattern_.size()) {
    z_[0] = 0;
    for (int i = 1; i < pattern.size(); ++i) {
      z_[i] = CountZ(pattern, i);
    }
    left_ = 0;
    right_ = 0;
  }
  void PutNext(char c) {
    buffer_[input_i_++ % buffer_.size()] = c;
    if (input_i_ < pattern_.size()) {
      return;
    }

    if (CountZ(buffer_, current_i_) == pattern_.size()) {
      out_(current_i_);
    }

    ++current_i_;
  }

 private:
  Output out_;
  std::string pattern_;
  std::string buffer_;
  std::vector<int> z_;
  int left_ = 0;
  int right_ = 0;
  int current_i_ = 0;
  int input_i_ = 0;

  int CountZ(const std::string& s, int i) {
    int z_current = 0;
    if (i >= right_) {
      z_current = FindCommonSubstring(s, i, pattern_, 0);
      left_ = i;
      right_ = i + z_current - 1;
    } else if (z_[i - left_] >= right_ - i + 1) {
      z_current = right_ - i + 1 +
                  FindCommonSubstring(s, right_ + 1, pattern_, right_ - i + 1);
      left_ = i;
      right_ = i + z_current - 1;
    } else {
      z_current = z_[i - left_];
    }
    return z_current;
  }
  int FindCommonSubstring(const std::string& s1, int i1, const std::string& s2,
                          int i2) {
    int common = 0;
    for (; i2 + common < s2.size() &&
           s1[(i1 + common) % s1.size()] == s2[i2 + common];
         ++common) {
    }
    return common;
  }
};

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::string pattern = "";
  char str = 0;
  std::cin >> pattern;
  SubstringsFinder finder(pattern, [](int x) { std::cout << x << ' '; });
  while (std::cin >> str) {
    finder.PutNext(str);
  }
  return 0;
}