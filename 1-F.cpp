#include <iostream>
#include <limits>
#include <string>
#include <vector>

class SuffixArray {
 public:
  SuffixArray(const std::string& s, char space_symbol = '#')
      : str_(s), space_(space_symbol), real_length_(s.size()) {
    AddSpaceSymbols(space_);
    suf_arr_.resize(str_.size());
    SortString();
    for (size_t i = 1; i < str_.size(); i <<= 1) {
      StepOfSort(i);
    }
    FindInverseSuffixArray();
    CountLCP();
  }

 protected:
  static const int NUM_OF_CHARS = 256;
  std::string str_;
  char space_;
  size_t real_length_;
  std::vector<int> suf_arr_;
  std::vector<int> pockets_;
  std::vector<int> classes_;

  std::vector<int> inverse_suf_arr_;
  std::vector<int64_t> lcp_;

  void AddSpaceSymbols(char space) {
    size_t k = 1;
    for (; k <= real_length_; k <<= 1)
      ;
    str_ += std::string(k - real_length_, space);
  }

  void SortString() {
    std::vector<int> count(NUM_OF_CHARS);
    for (size_t i = 0; i < str_.size(); ++i) {
      ++count[str_[i]];
    }
    for (size_t i = 0, last = 0; i < NUM_OF_CHARS; ++i) {
      if (count[i]) {
        last = (count[i] += last);
      }
    }
    for (int i = str_.size() - 1; i >= 0; --i) {
      suf_arr_[--count[str_[i]]] = i;
    }
    classes_.resize(suf_arr_.size());
    classes_[suf_arr_[0]] = 0;
    for (size_t i = 1, num_of_class = 0; i < str_.size(); ++i) {
      if (str_[suf_arr_[i]] != str_[suf_arr_[i - 1]]) {
        pockets_.push_back(i);
        ++num_of_class;
      }
      classes_[suf_arr_[i]] = num_of_class;
    }
    pockets_.push_back(suf_arr_.size());
  }

  inline int& Elem(std::vector<int>& arr, int i) { return arr[i % arr.size()]; }

  inline bool CheckStrings(int i, int shift, int length) {
    return Elem(classes_, Elem(suf_arr_, i) + shift) ==
           Elem(classes_, Elem(suf_arr_, i + 1) + shift);
  }

  void UpdateClassesAndPockets(int length) {
    std::vector<int> newclasses(str_.size());
    pockets_.clear();
    classes_[suf_arr_[0]] = 0;
    for (size_t i = 1, cur_class = 0; i < str_.size(); ++i) {
      if (!(CheckStrings(i - 1, 0, length) &&
            CheckStrings(i - 1, length, length))) {
        ++cur_class;
        pockets_.push_back(i);
      }
      newclasses[suf_arr_[i]] = cur_class;
    }
    pockets_.push_back(suf_arr_.size());
    classes_ = std::move(newclasses);
  }

  void StepOfSort(int length) {
    for (int& x : suf_arr_) {
      x = (x - length + suf_arr_.size()) % suf_arr_.size();
    }
    std::vector<int> newarr(suf_arr_.size());
    for (int i = suf_arr_.size() - 1; i >= 0; --i) {
      newarr[--pockets_[classes_[suf_arr_[i]]]] = suf_arr_[i];
    }
    suf_arr_ = std::move(newarr);
    UpdateClassesAndPockets(length);
  }

  void FindInverseSuffixArray() {
    inverse_suf_arr_.resize(str_.size());
    for (size_t i = 0; i < str_.size(); ++i) {
      inverse_suf_arr_[suf_arr_[i]] = i;
    }
  }

  void CountLCP() {
    lcp_.resize(suf_arr_.size() - 1);
    for (int64_t i = 0, cur_lcp = 0; i < str_.size(); ++i) {
      if (inverse_suf_arr_[i] + 1 < static_cast<int>(suf_arr_.size())) {
        for (size_t j = suf_arr_[inverse_suf_arr_[i] + 1];
             i + cur_lcp < str_.size() && j + cur_lcp < str_.size() &&
             str_[i + cur_lcp] == str_[j + cur_lcp];
             ++cur_lcp)
          ;
        lcp_[inverse_suf_arr_[i]] = cur_lcp;
      }
      cur_lcp = (cur_lcp ? cur_lcp - 1 : 0);
    }
  }
};

class CommonSuffixArray : private SuffixArray {
 public:
  CommonSuffixArray(const std::string& s1, const std::string& s2)
      : SuffixArray(s1 + '$' + s2, '#'), s0_(s1), s1_(s2) {}

  std::string GetCommonSubstring(int64_t k) const;

 private:
  std::string s0_;
  std::string s1_;

  bool NumOfStr(int i) const;
};

inline bool CommonSuffixArray::NumOfStr(int i) const { return i > s0_.size(); }

std::string CommonSuffixArray::GetCommonSubstring(int64_t k) const {
  int i = 0;
  for (; i < suf_arr_.size() &&
         (suf_arr_[i] == s0_.size() || suf_arr_[i] > s0_.size() + s1_.size());
       ++i)
    ;
  int64_t n = 0;
  int64_t min_lcp = 0;
  int j = 0;
  do {
    j = i + 1;
    min_lcp = lcp_[i - 1];
    for (;
         j < suf_arr_.size() && NumOfStr(suf_arr_[i]) == NumOfStr(suf_arr_[j]);
         ++j) {
      min_lcp = std::min(min_lcp, lcp_[j - 1]);
    }
    min_lcp = std::min(min_lcp, lcp_[j - 1]);
    n += lcp_[j - 1] - min_lcp;
    i = j;
  } while (i < suf_arr_.size() && n < k);
  return (i < suf_arr_.size() ? str_.substr(suf_arr_[j], lcp_[j - 1] + k - n)
                              : "");
}

int main() {
  std::string s1;
  std::string s2;
  std::cin >> s1 >> s2;
  CommonSuffixArray arr(s1, s2);
  std::string substr;
  int64_t num = 0;
  std::cin >> num;
  std::cout << ((substr = arr.GetCommonSubstring(num)) != "" ? substr : "-1")
            << std::endl;
  return 0;
}