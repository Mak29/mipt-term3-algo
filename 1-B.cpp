#include <iostream>
#include <string>
#include <vector>

class PalindromesCounter {
 public:
  static int64_t CountPalindromes(const std::string& s) {
    if (!s.size()) {
      return 0;
    }

    // the array at index i stores the number of palindromes centered at index
    // i/2 (half means that the center is between adjacent integers)
    std::vector<int64_t> num_of_palindromes(2 * s.size() - 1);
    int64_t count = 0;
    num_of_palindromes[0] = 0;
    int l = 0;
    int r = 0;
    for (int i = 1; static_cast<size_t>(i) < 2 * s.size() - 1; ++i) {
      if (r <= i / 2) {
        num_of_palindromes[i] =
            CountSymmetricSymbols(s, LeftCenter(i), RightCenter(i));
        if (num_of_palindromes[i] != 0) {
          count += num_of_palindromes[i];
          l = RightCenter(i) - num_of_palindromes[i];
          r = LeftCenter(i) + num_of_palindromes[i];
        }
      } else if (num_of_palindromes[SymmetricIndexInPalindrome(l, r, i)] +
                     i / 2 <
                 r) {
        num_of_palindromes[i] =
            num_of_palindromes[SymmetricIndexInPalindrome(l, r, i)];
        count += num_of_palindromes[i];
      } else {
        num_of_palindromes[i] =
            r - i / 2 + CountSymmetricSymbols(s, i - r - 1, r + 1);
        if (num_of_palindromes[i] != 0) {
          count += num_of_palindromes[i];
          l = RightCenter(i) - num_of_palindromes[i];
          r = LeftCenter(i) + num_of_palindromes[i];
        }
      }
    }
    return count;
  }

 private:
  // i1 moves left, i2 moves right
  static int CountSymmetricSymbols(const std::string& s, int i1, int i2) {
    int d = -(i1 == i2);
    for (; static_cast<size_t>(i2) < s.size() && i1 >= 0 && s[i1] == s[i2];
         ++d, --i1, ++i2) {
    }
    return d;
  }

  static inline int LeftCenter(int i) { return (i - i % 2) / 2; }

  static inline int RightCenter(int i) { return (i + i % 2) / 2; }

  static inline int SymmetricIndexInPalindrome(int l, int r, int i) {
    return (2 * (l + r) - i);
  }
};

int main() {
  std::string str = "";
  std::cin >> str;
  std::cout << PalindromesCounter::CountPalindromes(str) << std::endl;
  return 0;
}