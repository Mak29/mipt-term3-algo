#include <algorithm>
#include <cassert>
#include <cmath>
#include <iostream>
#include <optional>
#include <set>
#include <vector>

namespace Cmp {
const double EPS = 1e-10;
inline bool Zero(double x) { return fabs(x) < EPS; }
inline bool Equal(double x, double y) { return Zero(x - y); }
inline bool Positive(double x) { return x > EPS; }
inline bool Negative(double x) { return Positive(-x); }
inline bool Less(double x, double y) { return Positive(y - x); }
inline bool More(double x, double y) { return Negative(y - x); }
inline bool LessOrEqual(double x, double y) { return !More(x, y); }
inline bool MoreOrEqual(double x, double y) { return !Less(x, y); }
}  // namespace Cmp

double Det(double a, double b, double c, double d) { return a * d - b * c; }

struct IndexSegment;

class Vector {
 public:
  Vector() = default;
  Vector(double x, double y) : x_(x), y_(y) {}
  explicit operator bool() const {
    return static_cast<bool>((*this) * (*this));
  }

  Vector& operator+=(const Vector& other);
  Vector& operator-=(const Vector& other);
  Vector& operator*=(const double& k);

  Vector operator+(const Vector& other) const;
  Vector operator-(const Vector& other) const;
  Vector operator*(const double& k) const;
  double operator*(const Vector& other) const;

  double x_;
  double y_;
};
class Segment : public Vector {
 public:
  Segment(const Vector& a, const Vector& b) : Vector(b - a), p_(a) {}
  inline Vector Begin() const { return p_; }
  inline Vector End() const { return p_ + static_cast<const Vector&>(*this); }

  double GetY(double x) const;

  friend bool Intersected(const Segment& a, const Segment& b);

 private:
  Vector p_;
};

double SignArea(const Vector& one, const Vector& two) {
  return Det(one.x_, one.y_, two.x_, two.y_);
}

double Segment::GetY(double x) const {
  if (Cmp::Equal(x, p_.x_)) {
    return p_.y_;
  }
  return p_.y_ + y_ / x_ * (x - p_.x_);
}

inline bool HaveDiffSign(const double& a, const double& b) {
  return Cmp::Zero(a) || Cmp::Zero(b) || (Cmp::Positive(a) ^ Cmp::Positive(b));
}

bool SegmentIntersectLine(const Segment& segment, const Segment& line) {
  return HaveDiffSign(SignArea(segment.Begin() - line.Begin(), line),
                      SignArea(segment.End() - line.Begin(), line));
}

bool Intersected(const Segment& a, const Segment& b) {
  return (std::max(a.Begin().x_, b.Begin().x_) <=
          std::min(a.End().x_, b.End().x_)) &&
         (std::max(std::min(a.Begin().y_, a.End().y_),
                   std::min(b.Begin().y_, b.End().y_)) <=
          std::min(std::max(a.Begin().y_, a.End().y_),
                   std::max(b.Begin().y_, b.End().y_))) &&
         SegmentIntersectLine(a, b) && SegmentIntersectLine(b, a);
}

Vector& Vector::operator+=(const Vector& other) {
  x_ += other.x_;
  y_ += other.y_;
  return *this;
}
Vector& Vector::operator-=(const Vector& other) {
  x_ -= other.x_;
  y_ -= other.y_;
  return *this;
}
Vector& Vector::operator*=(const double& k) {
  x_ *= k;
  y_ *= k;
  return *this;
}

Vector Vector::operator+(const Vector& other) const {
  Vector temp = *this;
  return temp += other;
}
Vector Vector::operator-(const Vector& other) const {
  Vector temp = *this;
  return temp -= other;
}
Vector Vector::operator*(const double& k) const {
  Vector temp = *this;
  return temp *= k;
}
double Vector::operator*(const Vector& other) const {
  return x_ * other.x_ + y_ * other.y_;
}

struct IndexSegment {
  IndexSegment() = default;
  IndexSegment(const std::vector<Segment>* ptr, int index)
      : arr(ptr), i(index) {}
  const std::vector<Segment>* arr = nullptr;
  int i = -1;
  inline const Segment& Get() const { return (*arr)[i]; }
  bool operator<(const IndexSegment& other) const {
    double x = std::max(Get().Begin().x_, other.Get().Begin().x_);
    double y1 = Get().GetY(x);
    double y2 = other.Get().GetY(x);
    if (Cmp::Equal(y1, y2)) {
      return i < other.i;
    }
    return Cmp::Less(y1, y2);
  }
};

struct Event {
  Event() = default;
  Event(bool b, const IndexSegment& ind) : is_begin(b), index(ind) {}
  bool is_begin = true;
  IndexSegment index;
  inline Vector Point() const;
  bool operator<(const Event& other) const;
};
inline Vector Event::Point() const {
  return (is_begin ? index.Get().Begin() : index.Get().End());
}

std::vector<Event> CreateEvents(const std::vector<Segment>* ptr_segments) {
  const std::vector<Segment>& segments = (*ptr_segments);
  std::vector<Event> events(2 * segments.size());
  for (int i = 0; i < segments.size(); ++i) {
    events[2 * i] = Event(true, IndexSegment(ptr_segments, i));
    events[2 * i + 1] = Event(false, IndexSegment(ptr_segments, i));
  }
  std::sort(events.begin(), events.end());
  return events;
}
bool Event::operator<(const Event& other) const {
  if (Cmp::Equal(Point().x_, other.Point().x_)) {
    return is_begin > other.is_begin;
  }
  return Cmp::Less(Point().x_, other.Point().x_);
}

std::pair<int, int> sort_pair(int a, int b) {
  return {std::min(a, b), std::max(a, b)};
}

std::optional<std::pair<int, int>> CheckInsert(
    const std::set<IndexSegment>& current,
    std::set<IndexSegment>::iterator it) {
  if (it != current.begin()) {
    auto prev = (--it)++;
    if (Intersected(prev->Get(), it->Get())) {
      return sort_pair(it->i, prev->i);
    }
  }
  auto next = (++it)--;
  if (next != current.end()) {
    if (Intersected(it->Get(), next->Get())) {
      return sort_pair(it->i, next->i);
    }
  }
  return {};
}

std::optional<std::pair<int, int>> CheckErase(
    const std::set<IndexSegment>& current,
    std::set<IndexSegment>::iterator it) {
  auto next = (++it)--;
  if (it != current.begin() && next != current.end()) {
    auto prev = (--it)++;
    if (Intersected(prev->Get(), next->Get())) {
      return sort_pair(prev->i, next->i);
    }
  }
  return {};
}

std::optional<std::pair<int, int>> IntersectedSegments(
    const std::vector<Segment>& segments) {
  std::set<IndexSegment> current_segments;
  const std::vector<Segment>* ptr = &segments;
  std::vector<Event> events = CreateEvents(ptr);
  for (const Event& event : events) {
    if (event.is_begin) {
      auto [it, f] = current_segments.insert(event.index);
      auto result = CheckInsert(current_segments, it);
      if (result) {
        return result;
      }
    } else {
      auto it = current_segments.find(event.index);
      auto result = CheckErase(current_segments, it);
      if (result) {
        return result;
      }
      current_segments.erase(it);
    }
  }
  return {};
}

int main() {
  freopen("input.txt", "r", stdin);
  std::vector<Segment> segments;
  int n = 0;
  std::cin >> n;
  for (int i = 0; i < n; ++i) {
    double x1 = 0;
    double y1 = 0;
    double x2 = 0;
    double y2 = 0;
    std::cin >> x1 >> y1 >> x2 >> y2;
    if (x1 < x2) {
      segments.emplace_back(Vector(x1, y1), Vector(x2, y2));
    } else {
      segments.emplace_back(Vector(x2, y2), Vector(x1, y1));
    }
  }
  std::optional<std::pair<int, int>> result = IntersectedSegments(segments);
  if (result) {
    std::cout << "YES\n"
              << result.value().first + 1 << ' ' << result.value().second + 1
              << std::endl;
  } else {
    std::cout << "NO" << std::endl;
  }
  return 0;
}