#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <unordered_map>
#include <vector>

template <typename T>
std::ostream& operator<<(std::ostream& out, const std::vector<T>& v) {
  for (auto x : v) {
    out << x << ' ';
  }
  std::cout << std::endl;
  return out;
}

class Aho_Corasick {
 public:
  Aho_Corasick() = delete;
  Aho_Corasick(const Aho_Corasick&) = delete;
  Aho_Corasick& operator=(const Aho_Corasick&) = delete;
  Aho_Corasick(const std::vector<std::string>& templates, char delimeter = '?')
      : template_(templates) {
    all_nodes_.push_back(root_ = new Node);
    current_state_ = root_;
    Node* condition = root_;
    for (size_t i = 0; i < template_.size(); ++i) {
      for (char c : template_[i]) {
        condition = AddNode(condition, c);
      }
      condition->num_template = i;
      condition = root_;
    }

    std::queue<Edge> bfs;
    bfs.emplace(root_, root_, 0);
    while (!bfs.empty()) {
      AddRef(bfs.front());
      for (auto next : bfs.front().current->go) {
        bfs.emplace(next.second, bfs.front().current, next.first);
      }
      bfs.pop();
    }
  }

  void Go(char c) { current_state_ = GoFromNode(current_state_, c); }

  std::vector<std::string> FindSubstrings() {
    std::vector<std::string> out;
    if (current_state_->num_template != -1) {
      out.push_back(template_[current_state_->num_template]);
    }
    for (Node* copy = current_state_->term_suffix_link; copy != root_;
         copy = copy->term_suffix_link) {
      out.push_back(template_[copy->num_template]);
    }
    return out;
  }

  ~Aho_Corasick() {
    for (auto ptr : all_nodes_) {
      delete ptr;
    }
  }

 private:
  struct Node {
    std::unordered_map<char, Node*> go;
    Node* suffix_link = nullptr;
    Node* term_suffix_link = nullptr;
    int num_template = -1;
    Node() = default;
    Node(int num_template) : num_template(num_template) {}
  };

  struct Edge {
    Node* current = nullptr;
    Node* parent = nullptr;
    char symbol = 0;

    Edge(Node* current = nullptr, Node* parent = nullptr, char c = 0)
        : current(current), parent(parent), symbol(c) {}
  };

  Node* root_ = nullptr;
  Node* current_state_ = nullptr;
  std::vector<std::string> template_;
  std::vector<Node*> all_nodes_;

  Node* AddNode(Node* node, char c, int num_template = -1) {
    auto search = node->go.find(c);
    if (search == node->go.end()) {
      all_nodes_.push_back(new Node(num_template));
      return node->go[c] = all_nodes_.back();
    } else {
      return search->second;
    }
  }
  void AddRef(const Edge& edge) {
    if (edge.parent == root_) {
      edge.current->suffix_link = root_;
      edge.current->term_suffix_link = root_;
      return;
    }
    edge.current->suffix_link =
        GoFromNode(edge.parent->suffix_link, edge.symbol);
    edge.current->term_suffix_link =
        (edge.current->suffix_link->num_template == -1
             ? edge.current->suffix_link->term_suffix_link
             : edge.current->suffix_link);
  }
  Node* GoFromNode(Node* node, char c) {
    auto search = node->go.find(c);
    if (search == node->go.end()) {
      return (node == root_ ? root_
                            : node->go[c] = GoFromNode(node->suffix_link, c));
    }
    return search->second;
  }
};

class TemplateFinder {
 public:
  TemplateFinder() = delete;
  TemplateFinder(const std::string& t, char delimiter = '?')
      : index_(),
        num_of_substrings_(0),
        words_finder_(Divide(t)),
        length_(t.size()),
        delimiter_(delimiter) {}

  std::vector<int> FindSubstrings(const std::string& s) {
    counts_of_substrings_.resize(s.size(), 0);
    for (int i = 0; static_cast<size_t>(i) < s.size(); ++i) {
      words_finder_.Go(s[i]);
      std::vector<std::string> finded = words_finder_.FindSubstrings();
      for (std::string word : finded) {
        for (int pos : index_[word]) {
          if (i - pos >= 0) {
            ++counts_of_substrings_[i - pos];
          }
        }
      }
    }
    std::vector<int> positions;
    for (int i = 0; i <= static_cast<int>(s.size()) - length_; ++i) {
      if (counts_of_substrings_[i] == num_of_substrings_) {
        positions.push_back(i);
      }
    }
    counts_of_substrings_.clear();
    return positions;
  }

 private:
  std::unordered_map<std::string, std::vector<int>> index_;
  int num_of_substrings_;
  Aho_Corasick words_finder_;
  std::vector<int> counts_of_substrings_;
  int length_;
  char delimiter_;

  std::vector<std::string> Divide(const std::string& t) {
    std::vector<std::string> parts;
    std::string s = "";
    for (int i = 0; static_cast<size_t>(i) < t.size();) {
      for (; static_cast<size_t>(i) < t.size() && t[i] != delimiter_; ++i) {
        s += t[i];
      }
      if (!s.empty()) {
        index_[s].push_back(i - 1);
        s = "";
        ++num_of_substrings_;
      } else {
        ++i;
      }
    }
    for (auto x : index_) {
      parts.push_back(x.first);
    }
    return parts;
  }
};

int main() {
  std::string pattern = "";
  std::string str = "";
  std::cin >> pattern >> str;
  TemplateFinder finder(pattern);
  std::cout << finder.FindSubstrings(str);

  return 0;
}
