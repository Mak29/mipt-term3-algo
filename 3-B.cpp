#include <cassert>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

const int FIELD_SIZE = 8;

class Game {
 public:
  Game(const std::vector<std::vector<bool>>& field) : field_(field) {
    moves_ = {{1, -1}, {0, -1}, {-1, -1}, {-1, 0},
              {-1, 1}, {0, 1},  {1, 1},   {1, 0}};

    for (Position esc; !esc.End(); ++esc) {
      for (Position term; !term.End(); ++term) {
        used_[esc.row][esc.col][term.row][term.col][false] = false;
        used_[esc.row][esc.col][term.row][term.col][true] = false;
        counted_[esc.row][esc.col][term.row][term.col][false] = 0;
        counted_[esc.row][esc.col][term.row][term.col][true] = 0;
        degree_[esc.row][esc.col][term.row][term.col][true] = 0;
        degree_[esc.row][esc.col][term.row][term.col][false] = 0;
        for (auto [di, dj] : moves_) {
          if (isValid(esc.row + di, esc.col + dj)) {
            ++degree_[esc.row][esc.col][term.row][term.col][true];
          }
          if (isValid(term.row + di, term.col + dj)) {
            ++degree_[esc.row][esc.col][term.row][term.col][false];
          }
        }
      }
    }
    for (Position esc; !esc.End(); ++esc) {
      for (Position term; !term.End(); ++term) {
        if (isValid(esc.row, esc.col) && isValid(term.row, term.col)) {
          if (esc.row == FIELD_SIZE - 1) {
            counted_[esc.row][esc.col][term.row][term.col][true] = 1;
            ++degree_[esc.row][esc.col][term.row][term.col][true];
          }
          if (Shoot(esc.row, esc.col, term.row, term.col)) {
            counted_[esc.row][esc.col][term.row][term.col][false] = -1;
            counted_[esc.row][esc.col][term.row][term.col][true] = -1;
          }
        }
      }
    }

    for (Position esc; !esc.End(); ++esc) {
      if (isValid(esc.row, esc.col)) {
        for (Position term; !term.End(); ++term) {
          if (isValid(term.row, term.col)) {
            if ((counted_[esc.row][esc.col][term.row][term.col][true] == 1 ||
                 counted_[esc.row][esc.col][term.row][term.col][true] == -1) &&
                !used_[esc.row][esc.col][term.row][term.col][true]) {
              Traversal(esc.row, esc.col, term.row, term.col,
                        counted_[esc.row][esc.col][term.row][term.col][true],
                        true);
            }
            if ((counted_[esc.row][esc.col][term.row][term.col][false] == 1 ||
                 counted_[esc.row][esc.col][term.row][term.col][false] == -1) &&
                !used_[esc.row][esc.col][term.row][term.col][false]) {
              Traversal(esc.row, esc.col, term.row, term.col,
                        counted_[esc.row][esc.col][term.row][term.col][false],
                        false);
            }
          }
        }
      }
    }
  }

  int Winner(int esc_row, int esc_col, int term_row, int term_col,
             bool who = true) {
    return (counted_[esc_row][esc_col][term_row][term_col][who] == 1 ? 1 : -1);
    // return counted_[esc_row][esc_col][term_row][term_col][who];
  }

 private:
  static const int NUM_OF_POS =
      FIELD_SIZE * FIELD_SIZE * FIELD_SIZE * FIELD_SIZE;
  struct Position {
    int row = 0;
    int col = 0;
    Position& operator++() {
      ++col;
      row += (col / FIELD_SIZE);
      col %= FIELD_SIZE;
      return *this;
    }
    bool End() const { return row == FIELD_SIZE; }
  };
  std::vector<std::vector<bool>> field_;
  int counted_[FIELD_SIZE][FIELD_SIZE][FIELD_SIZE][FIELD_SIZE][2];
  int degree_[FIELD_SIZE][FIELD_SIZE][FIELD_SIZE][FIELD_SIZE][2];
  bool used_[FIELD_SIZE][FIELD_SIZE][FIELD_SIZE][FIELD_SIZE][2];
  std::vector<std::pair<int, int>> moves_;

  void Traversal(int esc_row, int esc_col, int term_row, int term_col,
                 int result, bool who);

  bool Shoot(int esc_row, int esc_col, int term_row, int term_col) const;

  bool isValid(int row, int col) const;
};

bool Game::Shoot(int esc_row, int esc_col, int term_row, int term_col) const {
  if (esc_row == term_row) {
    int begin = std::min(esc_col, term_col) + 1;
    int end = std::max(esc_col, term_col);
    bool f = true;
    for (int i = begin; i < end; ++i) {
      if (field_[esc_row][i]) {
        f = false;
      }
    }
    if (f) {
      return true;
    }
  } else if (esc_col == term_col) {
    int begin = std::min(esc_row, term_row) + 1;
    int end = std::max(esc_row, term_row);
    bool f = true;
    for (int i = begin; i < end; ++i) {
      if (field_[i][esc_col]) {
        f = false;
      }
    }
    if (f) {
      return true;
    }
  } else if (esc_col - esc_row == term_col - term_row) {
    int begin_col = std::min(esc_col, term_col);
    int begin_row = std::min(esc_row, term_row);
    int dist = abs(esc_col - term_col);
    bool f = true;
    for (int i = 1; i < dist; ++i) {
      if (field_[begin_row + i][begin_col + i]) {
        f = false;
      }
    }
    if (f) {
      return true;
    }
  } else if (esc_col + esc_row == term_col + term_row) {
    int begin_col = std::min(esc_col, term_col);
    int begin_row = std::max(esc_row, term_row);
    int dist = abs(esc_col - term_col);
    bool f = true;
    for (int i = 1; i < dist; ++i) {
      if (field_[begin_row - i][begin_col + i]) {
        f = false;
      }
    }
    if (f) {
      return true;
    }
  }
  return false;
}

bool Game::isValid(int row, int col) const {
  return row >= 0 && col >= 0 && row < FIELD_SIZE && col < FIELD_SIZE &&
         !field_[row][col];
}

void Game::Traversal(int esc_row, int esc_col, int term_row, int term_col,
                     int result, bool who) {
  used_[esc_row][esc_col][term_row][term_col][who] = true;
  for (auto [shift_row, shift_col] : moves_) {
    int new_esc_row = esc_row + (!who ? shift_row : 0);
    int new_esc_col = esc_col + (!who ? shift_col : 0);
    int new_term_row = term_row + (who ? shift_row : 0);
    int new_term_col = term_col + (who ? shift_col : 0);
    if (isValid(new_esc_row, new_esc_col) &&
        isValid(new_term_row, new_term_col) &&
        !used_[new_esc_row][new_esc_col][new_term_row][new_term_col][!who]) {
      if (counted_[new_esc_row][new_esc_col][new_term_row][new_term_col]
                  [!who] == 0 &&
              (!who && result == 1) ||
          (who && result == -1) ||
          ((--degree_[new_esc_row][new_esc_col][new_term_row][new_term_col]
                     [!who]) <= 0)) {
        counted_[new_esc_row][new_esc_col][new_term_row][new_term_col][!who] =
            result;
        Traversal(new_esc_row, new_esc_col, new_term_row, new_term_col, result,
                  !who);
      }
    }
  }
}

void HandleCell(char c, std::vector<std::vector<bool>>& field, int i,
                int j, int& esc_row, int& esc_col, int& term_row,
                int& term_col) {
  switch (c) {
    case '2':
      esc_row = i;
      esc_col = j;
      field[i][j] = false;
      return;
    case '3':
      term_row = i;
      term_col = j;
      field[i][j] = false;
      return;
    default:
      field[i][j] = (c == '1');
      return;
  }
}

int main() {
  int escaper_row = 0;
  int escaper_col = 0;
  int terminator_row = 0;
  int terminator_col = 0;
  std::vector<std::vector<bool>> field(FIELD_SIZE,
                                       std::vector<bool>(FIELD_SIZE));
  for (int i = 0; i < FIELD_SIZE; ++i) {
    for (int j = 0; j < FIELD_SIZE; ++j) {
      char c = 0;
      std::cin >> c;
      HandleCell(c, field, i, j, escaper_row, escaper_col, terminator_row, terminator_col);
    }
  }
  Game g(field);
  std::cout << g.Winner(escaper_row, escaper_col, terminator_row,
                        terminator_col)
            << std::endl;
  return 0;
}