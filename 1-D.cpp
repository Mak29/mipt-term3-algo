#include <limits>
#include <iostream>
#include <string>
#include <vector>

class SuffixArray {
 public:
  SuffixArray(const std::string& s, char space_symbol = '#')
      : str_(s), space_(space_symbol), real_length_(s.size()) {
    AddSpaceSymbols(space_);
    suf_arr_.resize(str_.size());
    SortString();
    for (size_t i = 1; i < str_.size(); i <<= 1) {
      StepOfSort(i);
    }
    FindInverseSuffixArray();
    CountLCP();
  }

  int NumOfDifferentSubstrings() const {
    uint64_t count = 0;
    for (size_t i = str_.size() - real_length_; i < suf_arr_.size(); ++i) {
      count += real_length_ - suf_arr_[i] - lcp_[i - 1];
    }
    return count;
  }

 private:
  const int CHAR_SIZE = 256;
  std::string str_;
  char space_;
  size_t real_length_;
  std::vector<int> suf_arr_;
  std::vector<int> pockets_;
  std::vector<int> classes_;

  std::vector<int> inverse_suf_arr_;
  std::vector<int> lcp_;

  void AddSpaceSymbols(char space) {
    size_t k = 1;
    for (; k <= real_length_; k <<= 1)
      ;
    str_ += std::string(k - real_length_, space);
  }

  void SortString() {
    std::vector<int> count(CHAR_SIZE);
    for (size_t i = 0; i < str_.size(); ++i) {
      ++count[str_[i]];
    }
    for (size_t i = 0, last = 0; i < CHAR_SIZE; ++i) {
      if (count[i]) {
        last = (count[i] += last);
      }
    }
    for (int i = str_.size() - 1; i >= 0; --i) {
      suf_arr_[--count[str_[i]]] = i;
    }
    classes_.resize(suf_arr_.size());
    classes_[suf_arr_[0]] = 0;
    for (size_t i = 1, num_of_class = 0; i < str_.size(); ++i) {
      if (str_[suf_arr_[i]] != str_[suf_arr_[i - 1]]) {
        pockets_.push_back(i);
        ++num_of_class;
      }
      classes_[suf_arr_[i]] = num_of_class;
    }
    pockets_.push_back(suf_arr_.size());
  }

  inline int& Elem(std::vector<int>& arr, int i) { return arr[i % arr.size()]; }

  inline bool CheckSubtringsBelongToSameClasses(int i, int shift) {
    return Elem(classes_, Elem(suf_arr_, i) + shift) ==
           Elem(classes_, Elem(suf_arr_, i + 1) + shift);
  }

  void UpdateClassesAndPockets(int length) {
    std::vector<int> newclasses(str_.size());
    pockets_.clear();
    classes_[suf_arr_[0]] = 0;
    for (size_t i = 1, cur_class = 0; i < str_.size(); ++i) {
      if (!(CheckSubtringsBelongToSameClasses(i - 1, 0) &&
            CheckSubtringsBelongToSameClasses(i - 1, length))) {
        ++cur_class;
        pockets_.push_back(i);
      }
      newclasses[suf_arr_[i]] = cur_class;
    }
    pockets_.push_back(suf_arr_.size());
    classes_ = std::move(newclasses);
  }

  void StepOfSort(int length) {
    for (int& x : suf_arr_) {
      x = (x - length + suf_arr_.size()) % suf_arr_.size();
    }
    std::vector<int> newarr(suf_arr_.size());
    for (int i = suf_arr_.size() - 1; i >= 0; --i) {
      newarr[--pockets_[classes_[suf_arr_[i]]]] = suf_arr_[i];
    }
    suf_arr_ = std::move(newarr);
    UpdateClassesAndPockets(length);
  }

  void FindInverseSuffixArray() {
    inverse_suf_arr_.resize(str_.size());
    for (size_t i = 0; i < str_.size(); ++i) {
      inverse_suf_arr_[suf_arr_[i]] = i;
    }
  }

  void CountLCP() {
    lcp_.resize(suf_arr_.size() - 1);
    for (size_t i = 0, cur_lcp = 0; i < str_.size(); ++i) {
      if (inverse_suf_arr_[i] + 1 < static_cast<int>(suf_arr_.size())) {
        for (size_t j = suf_arr_[inverse_suf_arr_[i] + 1];
             i + cur_lcp < str_.size() && j + cur_lcp < str_.size() &&
             str_[i + cur_lcp] == str_[j + cur_lcp];
             ++cur_lcp)
          ;
        lcp_[inverse_suf_arr_[i]] = cur_lcp;
      }
      cur_lcp = (cur_lcp ? cur_lcp - 1 : 0);
    }
  }
};

void PrintSuffix(const std::string& s, size_t i) {
  if (i < s.size()) {
    std::cout << i << ": ";
    for (; i < s.size(); ++i) {
      std::cout << s[i];
    }
    std::cout << std::endl;
  }
}

int main() {
  std::string str;
  std::cin >> str;
  SuffixArray arr(str);
  std::cout << arr.NumOfDifferentSubstrings() << std::endl;
  return 0;
}