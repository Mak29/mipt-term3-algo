#include <algorithm>
#include <cmath>
#include <cstdint>
#include <ctime>
#include <fstream>
#include <iostream>
#include <optional>
#include <vector>

class ConvexHull;

class Vector {
 public:
  Vector() = default;
  Vector(int64_t x, int64_t y) : x_(x), y_(y) {}
  explicit operator bool() const {
    return static_cast<bool>((*this) * (*this));
  }

  Vector& operator+=(const Vector& other);
  Vector& operator-=(const Vector& other);

  Vector operator+(const Vector& other) const;
  Vector operator-(const Vector& other) const;
  int64_t operator*(const Vector& other) const;

  friend int64_t SignArea(const Vector& one, const Vector& two);

  friend std::ostream& operator<<(std::ostream& out, const Vector& a);

  long double Length() const;

  class BottomRightComparator {
  public:
    bool operator()(const Vector& one,
                    const Vector& two) const {
      if (one.y_ == two.y_) {
        return one.x_ > two.x_;
      }
      return one.y_ < two.y_;
    }
  };

 private:
  int64_t x_;
  int64_t y_;
};

Vector& Vector::operator+=(const Vector& other) {
  x_ += other.x_;
  y_ += other.y_;
  return *this;
}
Vector& Vector::operator-=(const Vector& other) {
  x_ -= other.x_;
  y_ -= other.y_;
  return *this;
}

Vector Vector::operator+(const Vector& other) const {
  Vector temp = *this;
  return temp += other;
}
Vector Vector::operator-(const Vector& other) const {
  Vector temp = *this;
  return temp -= other;
}
int64_t Vector::operator*(const Vector& other) const {
  return x_ * other.x_ + y_ * other.y_;
}

int64_t SignArea(const Vector& one, const Vector& two) {
  return one.x_ * two.y_ - one.y_ * two.x_;
}

long double Vector::Length() const { return sqrt((*this) * (*this)); }

class ConvexHull {
 public:
  ConvexHull(const std::vector<Vector>& points) : points_(points) {}
  std::vector<Vector> Find();

 private:
  class AngleComparator {
   public:
    AngleComparator(const Vector& point) : first_(point) {}
    bool operator()(const Vector& one, const Vector& two) const {
      Vector a = one - first_;
      Vector b = two - first_;
      if (!a) {
        return static_cast<bool>(b);
      }
      if (!b) {
        return false;
      }
      if (!SignArea(a, b)) {
        return ((a * a) < (b * b));
      }
      return (SignArea(a, b) > 0);
    }

   private:
    Vector first_;
  };

  std::vector<Vector> points_;
  bool NotConvex(const std::vector<Vector>& points, const Vector& new_p);
};

inline bool ConvexHull::NotConvex(const std::vector<Vector>& points,
                                  const Vector& new_p) {
  if (points.size() < 2) {
    return false;
  }
  return (SignArea(points.back() - points[points.size() - 2],
                   new_p - points.back()) <= 0);
}

std::vector<Vector> ConvexHull::Find() {
  if (!points_.size()) {
    return {};
  }
  // Vector first = points_[0];
  // for (size_t i = 1; i < points_.size(); ++i) {
  //   if (points_[i].y_ < first.y_) {
  //     first = points_[i];
  //   } else if (points_[i].y_ == first.y_ && points_[i].x_ > first.x_) {
  //     first = points_[i];
  //   }
  // }
  Vector::BottomRightComparator cmp_first;
  Vector first = *std::min_element(points_.begin(), points_.end(), cmp_first);
  AngleComparator cmp(first);
  std::sort(points_.begin(), points_.end(), cmp);
  std::vector<Vector> new_points;
  new_points.push_back(points_.front());
  for (size_t i = 1; i < points_.size() - 1; ++i) {
    if (SignArea(points_[i] - first, points_[i + 1] - first)) {
      new_points.push_back(points_[i]);
    }
  }
  new_points.push_back(points_.back());
  points_ = std::move(new_points);
  std::vector<Vector> result;
  result.push_back(first);
  for (size_t i = 1; i < points_.size(); ++i) {
    while (NotConvex(result, points_[i])) {
      result.pop_back();
    }
    result.push_back(points_[i]);
  }
  return result;
}

namespace my_geometry_util {
long double Perimeter(const std::vector<Vector>& points) {
  if (points.size() < 2) {
    return 0.;
  }
  if (points.size() == 2) {
    return (points[1] - points[0]).Length();
  }
  long double p = (points[0] - points.back()).Length();
  for (size_t i = 1; i < points.size(); ++i) {
    p += (points[i] - points[i - 1]).Length();
  }
  return p;
}
}  // namespace my_geometry_util

int main() {
  std::ifstream in("input.txt");
  int n = 0;
  in >> n;
  std::vector<Vector> points;
  for (int i = 0; i < n; ++i) {
    int64_t x = 0;
    int64_t y = 0;
    in >> x >> y;
    points.emplace_back(x, y);
  }
  ConvexHull ch(points);
  std::cout.precision(8);
  std::cout << std::fixed << my_geometry_util::Perimeter(ch.Find()) << std::endl;
  return 0;
}