#include <cmath>
#include <iostream>
#include <optional>
#include <vector>

const double EPS = 1e-10;
namespace Compare {
inline bool Zero(double x) { return fabs(x) < EPS; }
inline bool Equal(double x, double y) { return Zero(x - y); }
inline bool Positive(double x) { return x > EPS; }
inline bool Negative(double x) { return Positive(-x); }
inline bool Less(double x, double y) { return Positive(y - x); }
inline bool More(double x, double y) { return Negative(y - x); }
inline bool LessOrEqual(double x, double y) { return !More(x, y); }
inline bool MoreOrEqual(double x, double y) { return !Less(x, y); }
}  // namespace Compare

double Det(double a, double b, double c, double d) { return a * d - b * c; }

template <typename CoordType>
class Vector {
 public:
  Vector() = default;
  Vector(CoordType x, CoordType y, CoordType z) : x_(x), y_(y), z_(z) {}
  explicit operator bool() const { return !Compare::Zero((*this) * (*this)); }

  Vector& operator+=(const Vector& other);
  Vector& operator-=(const Vector& other);
  Vector& operator*=(const CoordType& k);

  Vector operator+(const Vector& other) const;
  Vector operator-(const Vector& other) const;
  Vector operator*(const CoordType& k) const;
  CoordType operator*(const Vector& other) const;

  template <typename T>
  friend Vector<T> VectorMul(const Vector<T>& one, const Vector<T>& two);
  template <typename T>
  friend T MixedMul(const Vector<T>& one, const Vector<T>& two,
                    const Vector<T>& three);

  bool Collinear(const Vector& other) const;

  template <typename T>
  friend std::ostream& operator<<(std::ostream& out, const Vector<T>& a);

  template <typename T>
  friend std::istream& operator>>(std::istream& in, Vector<T>& a);

  // only double functions

  CoordType Length() const;
  Vector& Normalize(int length = 1);

  Vector<double> Proection(const Vector<double>& v) const;

 private:
  CoordType x_;
  CoordType y_;
  CoordType z_;
};

class Plane;
class Segment;

class Line {
 public:
  Line(const Vector<double>& point, const Vector<double>& direction)
      : p_(point), d_(direction) {}
  Segment Dist(const Vector<double>& point) const;

  explicit operator Vector<double>() const { return d_; }

  bool operator==(const Line& other) const;
  bool operator!=(const Line& other) const;

  friend class Plane;
  friend std::optional<Vector<double>> Intersection(const Plane& a,
                                                    const Line& l);
  friend std::optional<Vector<double>> Intersection(const Line& l,
                                                    const Line& m);
  friend Segment Dist(const Line& l, const Line& m);

 private:
  Vector<double> p_;
  Vector<double> d_;
};

class Segment : public Vector<double> {
 public:
  Segment(const Vector<double>& a, const Vector<double>& b) : p_(a) {
    d_ = b - a;
  }
  explicit operator Line() const { return Line(p_, d_); }
  std::pair<Vector<double>, Vector<double>> Ends() const {
    return {p_, p_ + d_};
  }
  double Length() const;

  bool Has(const Vector<double>& point) const;
  Segment Dist(const Vector<double>& point) const;

  friend Segment Dist(const Segment& a, const Segment& b);

 private:
  Vector<double> p_;
  Vector<double>& d_ = static_cast<Vector<double>&>(*this);
};

class Plane {
 public:
  Plane(const Vector<double>& point, const Vector<double> normal);
  Plane(const Vector<double>& point, const Vector<double>& a,
        const Vector<double>& b);
  Plane(const Line& a, const Vector<double> point)
      : Plane(point, point - a.p_, a.d_) {}

  double Deviation(const Vector<double>& point) const;
  Segment Dist(const Vector<double>& point) const;

  Vector<double> Normal() const;

  friend std::optional<Vector<double>> Intersection(const Plane& a,
                                                    const Line& l);
  friend std::optional<Line> Intersection(const Plane& a, const Plane& b);

 private:
  Vector<double> p_;
  Vector<double> n_;
};

template <typename CoordType>
Vector<CoordType>& Vector<CoordType>::operator+=(
    const Vector<CoordType>& other) {
  x_ += other.x_;
  y_ += other.y_;
  z_ += other.z_;
  return *this;
}
template <typename CoordType>
Vector<CoordType>& Vector<CoordType>::operator-=(
    const Vector<CoordType>& other) {
  x_ -= other.x_;
  y_ -= other.y_;
  z_ -= other.z_;
  return *this;
}
template <typename CoordType>
Vector<CoordType>& Vector<CoordType>::operator*=(const CoordType& k) {
  x_ *= k;
  y_ *= k;
  z_ *= k;
  return *this;
}

template <typename CoordType>
Vector<CoordType> Vector<CoordType>::operator+(
    const Vector<CoordType>& other) const {
  Vector<CoordType> temp = *this;
  return temp += other;
}
template <typename CoordType>
Vector<CoordType> Vector<CoordType>::operator-(
    const Vector<CoordType>& other) const {
  Vector<CoordType> temp = *this;
  return temp -= other;
}
template <typename CoordType>
Vector<CoordType> Vector<CoordType>::operator*(const CoordType& k) const {
  Vector<CoordType> temp = *this;
  return temp *= k;
}
template <typename CoordType>
CoordType Vector<CoordType>::operator*(const Vector<CoordType>& other) const {
  return x_ * other.x_ + y_ * other.y_ + z_ * other.z_;
}

template <typename CoordType>
Vector<CoordType> VectorMul(const Vector<CoordType>& one,
                            const Vector<CoordType>& two) {
  CoordType new_x = Det(one.y_, one.z_, two.y_, two.z_);
  CoordType new_y = Det(one.z_, one.x_, two.z_, two.x_);
  CoordType new_z = Det(one.x_, one.y_, two.x_, two.y_);
  return Vector<CoordType>(new_x, new_y, new_z);
}
template <typename CoordType>
CoordType MixedMul(const Vector<CoordType>& one, const Vector<CoordType>& two,
                   const Vector<CoordType>& three) {
  return VectorMul(one, two) * three;
}

template <typename CoordType>
bool Vector<CoordType>::Collinear(const Vector& other) const {
  return !static_cast<bool>(VectorMul(*this, other));
}

template <>
double Vector<double>::Length() const {
  return sqrt((*this) * (*this));
}
template <>
Vector<double>& Vector<double>::Normalize(int length) {
  return (*this) *= (length / Length());
}

template <>
Vector<double> Vector<double>::Proection(const Vector<double>& v) const {
  return (*this) * ((v * (*this)) / ((*this) * (*this)));
}

double Area(const Vector<double>& one, const Vector<double>& two) {
  return VectorMul(one, two).Length();
}

// Line

bool Line::operator==(const Line& other) const {
  return d_.Collinear(other.d_) && d_.Collinear(p_ - other.p_);
}
bool Line::operator!=(const Line& other) const { return (*this) != other; }

Segment Line::Dist(const Vector<double>& point) const {
  Vector<double> d = (point - p_) - d_.Proection(point - p_);
  return {point - d, point};
}

std::optional<Vector<double>> Intersection(const Line& l, const Line& m) {
  if (Compare::Zero(MixedMul(l.d_, m.d_, l.p_ - m.p_))) {
    return {};
  }
  if (l.d_.Collinear(m.d_)) {
    return ((l.Dist(m.p_)).Length() ? std::nullopt : std::optional(l.p_));
  }
  return m.p_ - m.d_ * ((static_cast<Vector<double>>(l.Dist(m.p_)) * m.d_) /
                        (m.d_ * m.d_));
}

// Plane

Plane::Plane(const Vector<double>& point, const Vector<double> normal)
    : p_(point), n_(normal) {}
Plane::Plane(const Vector<double>& point, const Vector<double>& a,
             const Vector<double>& b)
    : p_(point), n_(VectorMul(a, b)) {}

Vector<double> Plane::Normal() const { return n_; }

double Plane::Deviation(const Vector<double>& point) const {
  return Dist(point) * n_ / n_.Length();
}
Segment Plane::Dist(const Vector<double>& point) const {
  Vector<double> d = n_.Proection(point - p_);
  return {point - d, point};
}

std::optional<Vector<double>> Intersection(const Plane& a, const Line& l) {
  if (Compare::Zero(a.n_ * l.d_)) {
    return (Compare::Zero(a.Deviation(l.p_)) ? std::optional(l.p_)
                                             : std::nullopt);
  }
  return l.p_ - (l.d_ * (a.Deviation(l.p_) / a.Deviation(l.d_ + a.p_)));
}

std::optional<Line> Intersection(const Plane& a, const Plane& b) {
  if (a.n_.Collinear(b.n_)) {
    return {};
  }
  double d = Det(a.n_ * a.n_, a.n_ * b.n_, b.n_ * a.n_, b.n_ * b.n_);
  double p = Det(a.p_ * a.n_, a.n_ * b.n_, b.p_ * b.n_, b.n_ * b.n_) / d;
  double q = Det(a.n_ * a.n_, a.n_ * b.n_, a.p_ * a.n_, a.n_ * b.n_) / d;
  return Line((a.n_ * p) + (b.n_ * q), VectorMul(a.n_, b.n_));
}

// Segment

Segment Dist(const Line& l, const Line& m) {
  Vector<double> n = VectorMul(l.d_, m.d_);
  if (!n) {
    return l.Dist(m.p_);
  }
  Vector<double> a = Intersection(Plane(m, n + m.p_), l).value();
  Vector<double> b = Intersection(Plane(l, n + l.p_), m).value();
  return {a, b};
}

bool Segment::Has(const Vector<double>& point) const { return !Dist(point); }

Segment Segment::Dist(const Vector<double>& point) const {
  if (Compare::LessOrEqual((point - p_) * d_, 0)) {
    return {p_, point};
  }
  if (Compare::MoreOrEqual((point - p_ - d_) * d_, 0)) {
    return {p_ + d_, point};
  }
  return Line(*this).Dist(point);
}

double Segment::Length() const { return d_.Length(); }

const Segment& Min(const Segment& a, const Segment& b) {
  return (Compare::Less(a.Length(), b.Length()) ? a : b);
}

Segment Dist(const Segment& a, const Segment& b) {
  Segment line_dist = Dist(Line(a), Line(b));

  if (a.Has(line_dist.Ends().first) && b.Has(line_dist.Ends().second)) {
    return line_dist;
  }
  return Min(Min(b.Dist(a.Ends().first), b.Dist(a.Ends().second)),
             Min(a.Dist(b.Ends().first), a.Dist(b.Ends().second)));
}

template <typename T>
std::ostream& operator<<(std::ostream& out, const Vector<T>& a) {
  out << a.x_ << ' ' << a.y_ << ' ' << a.z_ << std::endl;
  return out;
}

template <typename T>
std::istream& operator>>(std::istream& in, Vector<T>& a) {
  in >> a.x_ >> a.y_ >> a.z_;
  return in;
}

int main() {
  // freopen("input.txt", "r", stdin);
  Vector<double> a;
  std::cin >> a;
  Vector<double> b;
  std::cin >> b;
  Vector<double> c;
  std::cin >> c;
  Vector<double> d;
  std::cin >> d;
  Segment ab(a, b);
  Segment cd(c, d);
  std::cout.precision(8);
  if (!ab) {
    if (!cd) {
      std::cout << std::fixed << (a - c).Length() << std::endl;
    } else {
      std::cout << std::fixed << cd.Dist(a).Length() << std::endl;
    }
  } else {
    if (!cd) {
      std::cout << std::fixed << ab.Dist(c).Length() << std::endl;
    } else {
      std::cout << std::fixed << Dist(ab, cd).Length() << std::endl;
    }
  }
  return 0;
}