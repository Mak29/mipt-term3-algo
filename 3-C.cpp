#include <iostream>
#include <vector>

namespace sprague_grundy {
std::vector<int> TargetForTheFirstShoot(int n) {
  std::vector<int> sg(n);  // function sprague grundy
  sg[0] = sg[1] = 0;
  for (int i = 2; i < n; ++i) {
    std::vector<bool> value(i, false);
    value[sg[i - 1]] = true;
    value[sg[i - 2]] = true;
    for (int j = 3; j <= (i + 1) / 2; ++j) {
      value[sg[j - 1] ^ sg[i - j]] = true;
    }
    sg[i] = 0;
    for (; sg[i] < i && value[sg[i]]; ++sg[i])
      ;
  }
  std::vector<int> target;
  if (!sg[n - 1]) {
    target.push_back(1);
  }
  if (n > 2 && !sg[n - 2]) {
    target.push_back(2);
  }
  for (int i = 3; i < n - 1; ++i) {
    if (!(sg[i - 1] ^ sg[n - i])) {
      target.push_back(i);
    }
  }
  if (n > 3 && !sg[n - 2]) {
    target.push_back(n - 1);
  }
  if (!sg[n - 1]) {
    target.push_back(n);
  }
  return target;
}
}  // namespace sprague_grundy

int main() {
  int n = 0;
  std::cin >> n;
  std::vector<int> targets = sprague_grundy::TargetForTheFirstShoot(n);
  if (targets.size()) {
    std::cout << "Schtirlitz" << std::endl;
    for (auto x : targets) {
      std::cout << x << std::endl;
    }
  } else {
    std::cout << "Mueller" << std::endl;
  }
  return 0;
}