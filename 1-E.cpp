#include <algorithm>
#include <iostream>
#include <iterator>
#include <limits>
#include <map>
#include <string>
#include <vector>

struct OutVertex {
  int parent = 0;
  int num_of_str = 0;
  int begin = 0;
  int end = 0;
};

std::ostream& operator<<(std::ostream& out, const OutVertex& v) {
  out << v.parent << ' ' << v.num_of_str << ' ' << v.begin << ' ' << v.end;
  return out;
}

class SuffixTree {
 public:
  SuffixTree();

  void AddString(const std::string& s);

  template <typename OutputIter>
  void PrintTree(OutputIter& out) const {
    int counter = 0;
    Dfs(root_, counter, out);
  }

  int GetVerticesCount() const { return num_of_vertexes_; }

  ~SuffixTree() {
    Delete(root_);
    for (char c = 0; c < NUM_OF_LETTERS; ++c) {
      delete joker_->edges_[c];
    }
    delete joker_;
    delete current_;
  }

 private:
  static const int NUM_OF_LETTERS = 28;
  static const char END1 = '#';
  static const char END2 = '$';
  struct Vertex;
  struct Edge {
    SuffixTree::Vertex* to = nullptr;
    int begin = 0;
    int num_of_str;
    int length = std::numeric_limits<int>::max();

    Edge() = default;
    Edge(SuffixTree::Vertex* ptr, int begin, int num_of_str)
        : to(ptr), begin(begin), num_of_str(num_of_str) {}
    Edge(SuffixTree::Vertex* ptr, int begin, int num_of_str, int length)
        : to(ptr), begin(begin), num_of_str(num_of_str), length(length) {}
    inline char GetSymbol(const SuffixTree* tree, int index) {
      return tree->str_[num_of_str][begin + index];
    }
  };
  struct Vertex {
    std::vector<Edge*> edges_ = std::vector<Edge*>(NUM_OF_LETTERS, nullptr);
    Vertex* suff_link = nullptr;

    Vertex() = default;
    Vertex(Vertex* ptr) : suff_link(ptr) {}
    inline Edge*& edges(char c) {
      return edges_[(c == END1 ? 0 : (c == END2 ? 1 : c - 'a' + 2))];
    }
  };
  struct Position {
    SuffixTree::Vertex* vertex = nullptr;
    int shift = 0;
    char c = 0;
    Position() = default;
    Position(SuffixTree::Vertex* ptr, int shift, char c)
        : vertex(ptr), shift(shift), c(c) {}
    explicit operator bool() const { return vertex != nullptr; }
    inline bool IsInVertex() const { return !shift; }
    SuffixTree::Edge* Edge() const {
      return (IsInVertex() ? nullptr : vertex->edges(c));
    }
  };

  Vertex* joker_ = nullptr;
  Vertex* root_ = nullptr;

  int num_of_vertexes_ = 1;

  Position* current_ = nullptr;

  std::vector<std::string> str_;

  void AddSymbol(int index);

  bool CanMove(char c) const;

  void AddEdge(int index, int num_of_str);

  Vertex* Split();

  void Go(char c);

  void FastRide(const Position& old_pos);

  template <typename OutputIter>
  void Dfs(Vertex* v, int& p, OutputIter& out) const;

  void Delete(Vertex* v);
};

SuffixTree::SuffixTree() {
  joker_ = new Vertex();
  root_ = new Vertex(joker_);
  for (int c = 0; c < NUM_OF_LETTERS; ++c) {
    joker_->edges_[c] = new Edge(root_, -1, -1, 1);
  }
  current_ = new Position(root_, 0, 0);
}

void SuffixTree::AddString(const std::string& s) {
  str_.push_back(s);
  for (size_t i = 0; i < str_.back().size(); ++i) {
    AddSymbol(i);
  }
  current_->vertex = root_;
  current_->shift = 0;
  current_->c = 0;
}

void SuffixTree::AddSymbol(int index) {
  Position prev;
  char c = str_.back()[index];
  while (!CanMove(c)) {
    Position old_pos = *current_;
    Vertex* v = Split();
    v->edges(c) = new Edge(nullptr, index, str_.size() - 1);
    ++num_of_vertexes_;
    current_->vertex = v;
    current_->shift = 1;
    current_->c = c;

    if (prev && !(prev.vertex->suff_link)) {
      prev.vertex->suff_link = current_->vertex;
    }
    prev = (*current_);
    FastRide(old_pos);
  }
  if (prev && !(prev.vertex->suff_link)) {
    prev.vertex->suff_link = current_->vertex;
  }
  Go(c);
}

bool SuffixTree::CanMove(char c) const {
  if (current_->IsInVertex()) {
    return current_->vertex->edges(c);
  }
  return current_->Edge()->GetSymbol(this, current_->shift) == c;
}

SuffixTree::Vertex* SuffixTree::Split() {
  if (current_->IsInVertex()) {
    return current_->vertex;
  } else {
    Vertex* new_v = new Vertex;
    Edge* old_edge = current_->Edge();
    new_v->edges(old_edge->GetSymbol(this, current_->shift)) =
        new Edge(old_edge->to, old_edge->begin + current_->shift,
                 old_edge->num_of_str, old_edge->length - current_->shift);
    ++num_of_vertexes_;
    old_edge->length = current_->shift;
    old_edge->to = new_v;
    return new_v;
  }
}

void SuffixTree::FastRide(const SuffixTree::Position& old_pos) {
  current_->vertex = old_pos.vertex->suff_link;
  current_->shift = 0;
  current_->c = 0;

  if (old_pos.shift) {
    int i = 0;
    Edge* old_edge = old_pos.vertex->edges(old_pos.c);
    char c = old_edge->GetSymbol(this, i);
    Edge* next = current_->vertex->edges(c);
    while (next->to && i + next->length <= old_pos.shift) {
      current_->vertex = next->to;
      i += next->length;
      c = old_edge->GetSymbol(this, i);
      next = current_->vertex->edges(c);
    }
    current_->shift = old_pos.shift - i;
    current_->c = (current_->shift > 0 ? old_edge->GetSymbol(this, i) : 0);
  }
}

void SuffixTree::Go(char c) {
  if (current_->IsInVertex()) {
    current_->c = c;
    current_->shift = 1;
    if (current_->Edge()->length == 1) {
      current_->vertex = current_->Edge()->to;
      current_->c = 0;
      current_->shift = 0;
    }
  } else if (current_->shift + 1 < current_->Edge()->length) {
    ++current_->shift;
  } else {
    current_->vertex = current_->Edge()->to;
    current_->shift = 0;
    current_->c = 0;
  }
}

template <typename OutputIter>
void SuffixTree::Dfs(SuffixTree::Vertex* v, int& p,
                     OutputIter& out) const {
  int num_v = p;
  for (char c = 0; c < NUM_OF_LETTERS; ++c) {
    if (v->edges_[c]) {
      ++p;
      OutVertex vertex;
      if (v->edges_[c]->to != nullptr) {
        vertex.parent = num_v;
        vertex.num_of_str = v->edges_[c]->num_of_str;
        vertex.begin = v->edges_[c]->begin;
        vertex.end = v->edges_[c]->begin + v->edges_[c]->length;
        Dfs(v->edges_[c]->to, p, out);
      } else {
        vertex.parent = num_v;
        vertex.num_of_str = v->edges_[c]->num_of_str;
        vertex.begin = v->edges_[c]->begin;
        vertex.end = static_cast<int>(str_[v->edges_[c]->num_of_str].size());
      }
      *(out++) = vertex;
    }
  }
}

void SuffixTree::Delete(SuffixTree::Vertex* v) {
  if (v == nullptr) {
    return;
  }
  for (char c = 0; c < NUM_OF_LETTERS; ++c) {
    if (v->edges_[c]) {
      Delete(v->edges_[c]->to);
      delete v->edges_[c];
    }
  }
  delete v;
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(NULL);
  std::cout.tie(NULL);
  std::string s;
  std::string t;
  std::cin >> s >> t;
  SuffixTree suf_tree;
  suf_tree.AddString(s);
  suf_tree.AddString(t);
  std::cout << suf_tree.GetVerticesCount() << std::endl;
  std::ostream_iterator<OutVertex> out(std::cout, "\n");
  suf_tree.PrintTree<std::ostream_iterator<OutVertex>>(out);
  return 0;
}