#include <cmath>
#include <iostream>
#include <vector>

namespace Cmp {
const double EPS = 1e-10;
inline bool Zero(double x) { return fabs(x) < EPS; }
inline bool Equal(double x, double y) { return Zero(x - y); }
inline bool Positive(double x) { return x > EPS; }
inline bool Negative(double x) { return Positive(-x); }
}  // namespace Cmp

class Vector {
 public:
  Vector() = default;
  Vector(double x, double y) : x_(x), y_(y) {}
  explicit operator bool() const { return ((*this) * (*this)); }

  Vector& operator+=(const Vector& other);
  Vector& operator-=(const Vector& other);

  Vector operator+(const Vector& other) const;
  Vector operator-(const Vector& other) const;
  double operator*(const Vector& other) const;

  friend double SignArea(const Vector& one, const Vector& two);

  double Length() const;

  friend std::ostream& operator<<(std::ostream& out, const Vector& a);

  friend bool Compare(const Vector& a, const Vector& b);

 private:
  double x_;
  double y_;
};

Vector& Vector::operator+=(const Vector& other) {
  x_ += other.x_;
  y_ += other.y_;
  return *this;
}
Vector& Vector::operator-=(const Vector& other) {
  x_ -= other.x_;
  y_ -= other.y_;
  return *this;
}

Vector Vector::operator+(const Vector& other) const {
  Vector temp = *this;
  return temp += other;
}
Vector Vector::operator-(const Vector& other) const {
  Vector temp = *this;
  return temp -= other;
}
double Vector::operator*(const Vector& other) const {
  return x_ * other.x_ + y_ * other.y_;
}

double SignArea(const Vector& one, const Vector& two) {
  return one.x_ * two.y_ - one.y_ * two.x_;
}

double Vector::Length() const { return sqrt((*this) * (*this)); }

Vector& at(std::vector<Vector>& arr, int i) { return arr[i % arr.size()]; }

bool Compare(const Vector& a, const Vector& b) {
  if (a.y_ == b.y_) {
    return a.x_ < b.x_;
  }
  return a.y_ < b.y_;
}

Vector Convert(std::vector<Vector>& arr, int& first_i) {
  Vector first_point = arr[0];
  Vector min_point = arr[0];
  first_i = 0;
  for (size_t i = 1; i < arr.size(); ++i) {
    if (Compare(arr[i], min_point)) {
      first_i = i;
      min_point = arr[i];
    }
    arr[i - 1] = arr[i] - arr[i - 1];
  }
  arr.back() = first_point - arr.back();
  return min_point;
}

std::vector<Vector> Sum(std::vector<Vector> a, std::vector<Vector> b) {
  std::vector<Vector> result;
  int first_a = 0;
  int first_b = 0;
  result.emplace_back(Convert(a, first_a) + Convert(b, first_b));
  size_t i_a = 0;
  size_t i_b = 0;
  while (i_a < a.size() && i_b < b.size()) {
    double sign_area = SignArea(at(a, first_a + i_a), at(b, first_b + i_b));
    if (Cmp::Zero(sign_area)) {
      result.push_back(result.back() + at(a, first_a + i_a++) +
                       at(b, first_b + i_b++));
    } else if (Cmp::Positive(sign_area)) {
      result.push_back(result.back() + at(b, first_b + i_b++));
    } else {
      result.push_back(result.back() + at(a, first_a + i_a++));
    }
  }
  for (; i_a < a.size(); ++i_a) {
    result.push_back(result.back() + at(a, first_a + i_a));
  }
  for (; i_b < b.size(); ++i_b) {
    result.push_back(result.back() + at(b, first_b + i_b));
  }
  return result;
}

std::ostream& operator<<(std::ostream& out, const Vector& a) {
  out << a.x_ << ' ' << a.y_ << std::endl;
  return out;
}

bool CheckZero(const std::vector<Vector>& polygon) {
  bool f = true;
  Vector zero = Vector(0, 0);
  for (size_t i = 1; f && i < polygon.size(); ++i) {
    f = f && (Cmp::Negative(
                 SignArea(polygon[i] - polygon[i - 1], zero - polygon[i - 1])));
  }
  return f;
}

int main() {
  freopen("input.txt", "r", stdin);
  int n = 0;
  std::cin >> n;
  std::vector<Vector> a;
  for (int i = 0; i < n; ++i) {
    double x = 0;
    double y = 0;
    std::cin >> x >> y;
    a.emplace_back(-x, -y);
  }
  int m = 0;
  std::cin >> m;
  std::vector<Vector> b;
  for (int i = 0; i < m; ++i) {
    double x = 0;
    double y = 0;
    std::cin >> x >> y;
    b.emplace_back(x, y);
  }
  std::vector<Vector> sum = Sum(a, b);
  std::cout << (CheckZero(sum) ? "YES" : "NO") << std::endl;
  return 0;
}