#include <algorithm>
#include <cmath>
#include <iostream>
#include <map>
#include <vector>

namespace Compare {
const double EPS = 1e-10;
inline bool Zero(double x) { return fabs(x) < EPS; }
inline bool Negative(double x) { return -x > EPS; }
}  // namespace Compare
double Det(double a, double b, double c, double d) { return a * d - b * c; }

class Vector {
 public:
  Vector() = default;
  Vector(double x, double y, double z) : x(x), y(y), z(z) {}
  explicit operator bool() const { return !Compare::Zero((*this) * (*this)); }

  Vector& operator+=(const Vector& other);
  Vector& operator-=(const Vector& other);
  Vector& operator*=(const double& k);

  Vector operator+(const Vector& other) const;
  Vector operator-(const Vector& other) const;
  Vector operator*(const double& k) const;
  double operator*(const Vector& other) const;

  friend Vector VectorMul(const Vector& one, const Vector& two);

  bool Collinear(const Vector& other) const;

  friend std::ostream& operator<<(std::ostream& out, const Vector& a);

  // only double functions

  double Length() const;
  Vector& Normalize(int length = 1);

  Vector Projection(const Vector& v) const;

  double x;
  double y;
  double z;
};

Vector& Vector::operator+=(const Vector& other) {
  x += other.x;
  y += other.y;
  z += other.z;
  return *this;
}
Vector& Vector::operator-=(const Vector& other) {
  x -= other.x;
  y -= other.y;
  z -= other.z;
  return *this;
}
Vector& Vector::operator*=(const double& k) {
  x *= k;
  y *= k;
  z *= k;
  return *this;
}

Vector Vector::operator+(const Vector& other) const {
  Vector temp = *this;
  return temp += other;
}
Vector Vector::operator-(const Vector& other) const {
  Vector temp = *this;
  return temp -= other;
}
Vector Vector::operator*(const double& k) const {
  Vector temp = *this;
  return temp *= k;
}
double Vector::operator*(const Vector& other) const {
  return x * other.x + y * other.y + z * other.z;
}

Vector VectorMul(const Vector& one, const Vector& two) {
  double new_x = Det(one.y, one.z, two.y, two.z);
  double new_y = Det(one.z, one.x, two.z, two.x);
  double new_z = Det(one.x, one.y, two.x, two.y);
  return Vector(new_x, new_y, new_z);
}

double Vector::Length() const { return sqrt((*this) * (*this)); }

Vector& Vector::Normalize(int length) {
  return (Compare::Zero(Length()) ? *this : (*this) *= (length / Length()));
}

Vector Vector::Projection(const Vector& v) const {
  return (*this) * ((v * (*this)) / ((*this) * (*this)));
}

double Area(const Vector& one, const Vector& two) {
  return VectorMul(one, two).Length();
}

using Edge = std::pair<int, int>;

struct Face {
  Face(int i_a, int i_b, int i_c) : i_a(i_a), i_b(i_b), i_c(i_c) {}
  Face(std::pair<int, int> e, int i) : i_a(e.first), i_b(e.second), i_c(i) {}

  inline Vector Normal(const std::vector<Vector>& vertex,
                       const Vector& dir = Vector(0., 0., 0.)) const {
    Vector normal =
        VectorMul(vertex[i_b] - vertex[i_a], vertex[i_c] - vertex[i_a])
            .Normalize();
    if (Compare::Negative(normal * dir)) {
      return normal *= -1.;
    }
    return normal;
  }

  int i_a;
  int i_b;
  int i_c;
};

Face FirstFace(const std::vector<Vector>& vertex) {
  Edge cur(0, 0);
  for (int i = 1; i < vertex.size(); ++i) {
    if (vertex[i].z < vertex[cur.first].z) {
      cur.first = i;
    }
  }
  double min_cos =
      Vector(0., 0., 1.) * (vertex[cur.second] - vertex[cur.first]).Normalize();
  for (int i = 1; i < vertex.size(); ++i) {
    if (i != cur.first) {
      if (cur.first == cur.second ||
          Vector(0., 0., 1.) * (vertex[i] - vertex[cur.first]).Normalize() <
              min_cos) {
        min_cos =
            Vector(0., 0., 1.) * (vertex[i] - vertex[cur.first]).Normalize();
        cur.second = i;
      }
    }
  }
  Face face(cur, 0);
  Vector z(0., 0., 1.);
  for (int i = 1; i < vertex.size(); ++i) {
    if (cur.first == face.i_c || cur.second == face.i_c) {
      face.i_c = i;
    } else {
      Vector one = face.Normal(vertex);
      Vector two = Face(cur, i).Normal(vertex);
      if (fabs(one * z) < fabs(two * z)) {
        face.i_c = i;
      }
    }
  }
  return face;
}

std::vector<Face> ConvexHull(const std::vector<Vector>& vertex) {
  std::vector<Face> face;
  face.push_back(FirstFace(vertex));
  std::map<Edge, int> cur_edge;
  cur_edge[Edge(face[0].i_a, face[0].i_b)] = 0;
  cur_edge[Edge(face[0].i_a, face[0].i_c)] = 0;
  cur_edge[Edge(face[0].i_b, face[0].i_c)] = 0;
  while (cur_edge.size()) {
    auto [cur, num_face] = *cur_edge.begin();
    cur_edge.erase(cur_edge.begin());
    int num_v = -1;
    for (int i = 0; i < vertex.size(); ++i) {
      if (i != face[num_face].i_a && i != face[num_face].i_b &&
          i != face[num_face].i_c) {
        if (num_v == -1) {
          num_v = i;
        } else {
          Vector normal = face[num_face].Normal(
              vertex, vertex[i] - vertex[face[num_face].i_a]);
          Vector p;
          if (face[num_face].i_a != cur.first &&
              face[num_face].i_a != cur.second) {
            p = vertex[face[num_face].i_a];
          } else if (face[num_face].i_b != cur.first &&
                     face[num_face].i_b != cur.second) {
            p = vertex[face[num_face].i_b];
          } else {
            p = vertex[face[num_face].i_c];
          }
          p -= vertex[cur.first];
          Vector one = Face(cur, num_v).Normal(vertex, p);
          Vector two = Face(cur, i).Normal(vertex, p);
          if (one * normal < two * normal) {
            num_v = i;
          }
        }
      }
    }
    std::map<Edge, int>::iterator it;
    if ((it = cur_edge.find(Edge(cur.first, num_v))) != cur_edge.end() ||
        (it = cur_edge.find(Edge(num_v, cur.first))) != cur_edge.end()) {
      cur_edge.erase(it);
    } else {
      cur_edge[Edge(cur.first, num_v)] = face.size();
    }
    if ((it = cur_edge.find(Edge(cur.second, num_v))) != cur_edge.end() ||
        (it = cur_edge.find(Edge(num_v, cur.second))) != cur_edge.end()) {
      cur_edge.erase(it);
    } else {
      cur_edge[Edge(cur.second, num_v)] = face.size();
    }
    face.emplace_back(cur, num_v);
  }
  return face;
}

void solve() {}

int main() {
  freopen("input.txt", "r", stdin);
  int n = 0;
  std::cin >> n;
  std::vector<Vector> points;
  for (int i = 0; i < n; ++i) {
    int x = 0;
    int y = 0;
    int z = 0;
    std::cin >> x >> y >> z;
    points.emplace_back(x, y, z);
  }
  std::vector<Face> ch(ConvexHull(points));
  int q = 0;
  std::cin >> q;
  for (int i = 0; i < q; ++i) {
    Vector p;
    std::cin >> p.x >> p.y >> p.z;
    double min_dist =
        ch[0].Normal(points).Projection(p - points[ch[0].i_a]).Length();
    for (int i = 1; i < ch.size(); ++i) {
      min_dist = std::min(
          min_dist,
          ch[i].Normal(points).Projection(p - points[ch[i].i_a]).Length());
    }
    std::cout.precision(5);
    std::cout << std::fixed << min_dist << std::endl;
  }
  return 0;
}